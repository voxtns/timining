import { environment } from '../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormlyModule, FORMLY_CONFIG } from '@ngx-formly/core';
import { registerTranslateExtension } from './translate.extension';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AngularFireModule } from '@angular/fire';
import { YtPlayerAngularModule } from 'yt-player-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';

import { AdminComponent } from './pages/admin/admin.component';
import { LandingComponent as LandingComponentAdmin } from './pages/admin/landing/landing.component';

import { SidenavComponent } from './component/navigation/sidenav/sidenav.component';
import { TopnavComponent } from './component/navigation/topnav/topnav.component';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { FooterComponent as FooterComponentAdmin } from './component/navigation/footer/footer.component';
import { HeaderComponent as HeaderComponentAdmin } from './component/navigation/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from './component/modal/alert/alert.component';
import { CardComponent } from './formly/wrappers/card/card.component';
import { FormlyComponent } from './component/formly/formly.component';
import { SessionComponent } from './pages/session/session.component';
import { LoginComponent } from './pages/session/login/login.component';
import { CreateComponent } from './formly/create/create.component';
import { TocNavComponent } from './component/toc-nav/toc-nav.component';
import { VideoComponent } from './formly/video/video.component';
import { ViewVideoComponent } from './component/view-video/view-video.component';
import { StatusJobComponent } from './pages/admin/status-job/status-job.component';
import { ChartComponent } from './component/chart/chart.component';

export function createTranslateLoader(http: HttpClient) {
  let translate = new TranslateHttpLoader(http, './assets/i18n/', '.json');
  console.log(translate)
  return translate;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    HeaderComponent,
    FooterComponent,
    AdminComponent,
    SidenavComponent,
    TopnavComponent,
    LandingComponentAdmin,
    FooterComponentAdmin,
    HeaderComponentAdmin,
    AlertComponent,
    CardComponent,
    FormlyComponent,
    SessionComponent,
    CreateComponent,
    TocNavComponent,
    VideoComponent,
    ViewVideoComponent,
    StatusJobComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    YtPlayerAngularModule,
    AngularFireModule.initializeApp(environment.firebase),
    TranslateModule.forRoot({
      defaultLanguage: environment.translate.default,
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    FormlyModule.forRoot(
      {
        wrappers: [
          { name: 'card', component: CardComponent },
        ],
        types : [
          { name: 'create', component: CreateComponent },
          { name: 'video', component: VideoComponent },
        ]
      }
    ),
    FormlyBootstrapModule,
    FeatherModule.pick(allIcons),
    NgbModule
  ],
  providers: [
    { 
      provide: FORMLY_CONFIG, 
      multi: true, 
      useFactory: registerTranslateExtension, 
      deps: [TranslateService] 
    },
  ],
  exports: [
    FeatherModule
  ],
  bootstrap: [AppComponent]
})

export class AppModule { 

}
