export const global = {
  //img : "https://www.drimu.com/assets/images/logo.png",
  //img : "assets/img/image1.png",
  name: "TIMining",
  description: "Se parte del futuro",
  home: "/",
  navbar: [
      {
          name: "Inicio",
          url: "/",
          template: {
              name: "landing1",
              title: "Gestionamos talento de mano de obra femenina en oficios de construcción",
              subTile: "Somos un Marketplace que conecta a la industria de la construcción con mano de obra femenina calificada",
              //img   : "https://miro.medium.com/max/800/1*2PKF7wUEXmZr0Fd5u-c2mA.png",
              backgroundImg: "assets/img/news/foto_1.jpg",
              buttons: [
                  {
                      name: "Registrate",
                      url: "/register",
                      class: "btn-primary rounded-pill lift lift-sm",
                  },
                  {
                      name: "Descubre nuestros cursos",
                      url: "/cursos",
                      class: "btn-link",
                      icon: "fa-arrow-right"
                  }
              ]
          }
      },
      {
          "name": "Cursos",
          "url": "/cursos",
          "template": {
              name: "landing4",
              backgroundImg: "assets/img/news/TLMD-mujeres-construccion-foto-zaira-jornaleras-8-de-marzo-1.jpg",
              style: "background-position-y: inherit;",
          }
      },
      {
          "name": "Contactanos",
          "url": "/contact"
      },
      {
          name: "Nuestra empresa",
          description: "Descubre lo que drimu puede hacer por ti",
          img: "https://ak0.picdn.net/shutterstock/videos/7162420/thumb/1.jpg",
          children: [
              {
                  col: "col-lg-6",
                  nav: [
                      {
                          name: "Compañia",
                          nav: [
                              {
                                  name: "Quienes Somos",
                                  description: "Descubre todo lo que necesitas",
                                  url: "/about"
                              },
                              {
                                  name: "Socio conductor",
                                  template: {
                                      name: "landing1",
                                      title: "La mejor manera de viajar!",
                                      subTile: "Ahorra dinero y viaja más entretenido en auto compartido junto a compañeros de tu mismo trabajo, universidad y demás comunidades!",
                                      img: "https://miro.medium.com/max/800/1*2PKF7wUEXmZr0Fd5u-c2mA.png",
                                      backgroundImg: "https://images.unsplash.com/photo-1529400971008-f566de0e6dfc?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1600&h=1200&fit=crop&ixid=eyJhcHBfaWQiOjF9"
                                  },
                                  url: "/driver"
                              }
                          ]
                      },
                      {
                          name: "Empresas",
                          nav: [
                              {
                                  "name": "Como Funciona",
                                  "url": "/iswork"
                              },
                              {
                                  "name": "Precio & planes",
                                  "url": "/pricing"
                              }
                          ]
                      },
                  ]
              },
              {
                  col: "col-lg-6",
                  nav: [
                      {
                          name: "Social",
                          nav: [
                              {
                                  "name": "Quienes nos apoyan",
                                  "url": "/support"
                              },
                              {
                                  "name": "Nuestras logros",
                                  "url": "/achievement"
                              },
                              {
                                  "name": "Escribenos",
                                  "url": "/write"
                              }
                          ]
                      }
                  ]
              }
          ]
      }
  ],
  button: [
      {
          name: "Iniciar sesión",
          url: "/login",
          template: false
      }
  ]
};
  