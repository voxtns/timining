export const software : Array<any> = [
    {
        name : "formly.basicName",
        async : [
          {
            key : "category",
            api : ['categories'],
            convertion : {
              value : "id",
              display : "name"
            }
          },
          {
            key : "tags",
            api : ['tags'],
            convertion : {
              value : "id",
              display : "name" 
            }
          }
        ],
        group : [
            {
                key: 'title',
                type: 'input',
                className : "col-md-12",
                templateOptions: {
                  label: 'formly.title',
                  translate: true,
                  placeholder: 'formly.title',
                  required: true,
                }
            },
            {
                key: 'content',
                type: 'textarea',
                className : "col-md-12",
                templateOptions: {
                  label: 'formly.content',
                  translate: true,
                  placeholder: 'formly.content',
                  required: true,
                  rows: 15,
                  grow: false
                }
            },
            {
                key: 'excerpt',
                type: 'textarea',
                className : "col-md-12",
                templateOptions: {
                  label: 'formly.excerpt',
                  translate: true,
                  placeholder: 'formly.excerpt',
                  rows: 5,
                  grow: false
                }
            },
            {
              key: 'permalink',
              type: 'input',
              className : "col-md-12",
              templateOptions: {
                label: 'formly.permalink',
                translate: true,
                placeholder: 'formly.permalink',
                required: true,
              }
            },
            {
                key: 'metas._price.meta_value',
                type: 'textarea',
                className : "col-md-12",
                templateOptions: {
                  label: 'formly.detailPricing',
                  translate: true,
                  placeholder: 'formly.detailPricing',
                  required: true,
                  rows: 3,
                  grow: false
                }
            },
            {
              key: 'category',
              type: 'tags',
              className : "col-md-12",
              templateOptions: {
                label: 'formly.choise',
                translate: true,
                multiple: true,
                placeholder: 'formly.choise',
                required: true,
                autocomplete : true
              }
            },
            {
              key: 'tags',
              type: 'tags',
              className : "col-md-12",
              templateOptions: {
                label: 'formly.etiqueta',
                translate: true,
                placeholder: 'formly.etiqueta',
                required: true,
                limitItemsTo : true
              }
            },
        ]
    },
    {
        name : "formly.detailProduct",
        async : [
          {
            key : "languages",
            api : ['languages'],
            convertion : {
              value : "id",
              display : "name"
            }
          }
        ],
        group : [
          {
              key: 'languages',
              type: 'tags',
              className : "col-md-12",
              templateOptions: {
                label: 'formly.languages',
                translate: true,
                placeholder: 'formly.languages',
                required: true,
                autocomplete: true
              }
          },
          {
              key: 'prices',
              type: 'tags',
              className : "col-md-12",
              templateOptions: {
                type: 'number',
                label: 'formly.pricing',
                translate: true,
                placeholder: 'formly.pricing',
                required: true,
              }
          },
          {
            key: 'deployment',
            type: 'tags',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.software',
              translate: true,
              placeholder: 'formly.software',
              required: true,
            }
          },
          {
            key: 'industries',
            type: 'tags',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.industries',
              translate: true,
              placeholder: 'formly.industries',
              required: true,
            }
          },
          {
            key: 'country',
            type: 'tags',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.country',
              translate: true,
              placeholder: 'formly.country',
              required: true,
            }
          },
        ]
    },
    {
        name : "formly.functions",
        group : [
          {
            key: 'category_',
            type: 'input',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.category',
              translate: true,
              placeholder: 'formly.category',
              required: true,
            }
          },
          {
            key: 'metas._seo.meta_value.title',
            type: 'input',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.seoTitle',
              translate: true,
              placeholder: 'formly.seoTitle',
              required: true,
            }
          },
          {
            key: 'metas._seo.meta_value.description',
            type: 'textarea',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.seoDescription',
              translate: true,
              placeholder: 'formly.seoDescription',
              required: true,
              rows: 5,
              grow: false
            }
          },
          {
            key: 'metas._seo.meta_value.h1',
            type: 'textarea',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.seoH1',
              translate: true,
              placeholder: 'formly.seoH1',
              rows: 5,
              grow: false
            }
          },
        ]
    },
    {
      name : "formly.media",
      group : [
        {
            key: 'metas._video.meta_value',
            type: 'input',
            className : "col-md-12",
            templateOptions: {
              label: 'formly.urlVideo',
              translate: true,
              placeholder: 'formly.urlVideo',
              required: true,
            }
        },
        {
          key: 'metas._wp_images.meta_value.images',
          type: 'fileDrop',
          className : "col-md-12",
          templateOptions: {
            label: 'formly.urlVideo',
            translate: true,
            placeholder: 'formly.urlVideo',
            required: true,
          }
        },
      ]
    },
    {
      name : "formly.administration",
      async : [
        {
          key : "status",
          api : ['options'],
          convertion : {
            value : "code",
            label : "name"
          }
        }
      ],
      group : [ 
        {
          key: 'slug',
          type: 'input',
          className : "col-md-6",
          templateOptions: {
            label: 'formly.slug',
            placeholder: 'formly.slug',
            required: true,
            translate: true
          },
        },
        {
          key: 'template',
          type: 'select',
          className : "col-md-6",
          templateOptions: {
            label: 'formly.template',
            required: true,
            translate: true
          },
        },
        {
          key: 'author',
          type: 'tags',
          className : "col-md-6",
          templateOptions: {
            label: 'formly.author',
            placeholder: 'formly.author',
            required: true,
            translate: true,
            autocompleteObservable:["contents", "software", "user"],
            autocomplete: true
          },
        },
        {
          key: 'languageAvailabilty',
          type: 'input',
          className : "col-md-6",
          templateOptions: {
            label: 'formly.languageAvailabilty',
            placeholder: 'formly.languageAvailabilty',
            required: true,
            translate: true,
            autocomplete: true
          },
        },
        {
          key: 'status',
          type: 'select',
          className : "col-md-12",
          templateOptions: {
            label: 'formly.status',
            required: true,
            translate: true
          },
        },
        {
          key: 'meta._verified.meta_value',
          type: 'checkbox',
          className : "col-md-12",
          templateOptions: {
            label: 'formly.verified',
            placeholder: 'formly.verified',
            translate: true,
            onChange: (field, model) => {
              console.log(field, model)
            }
          },
        }
      ]
    },
];