export const login = [
    {
        key: 'email',
        type: 'input',
        templateOptions: {
            type : "email",
            placeholder: 'Nombre de usuario',
            required: true,
        }
    },
    {
        key: 'password',
        type: 'input',
        templateOptions: {
            type : "password",            
            placeholder: 'Contraseña',
            required: true,
        }
    }
]