import { FormlyFieldConfig } from '@ngx-formly/core';

export const page : Array<FormlyFieldConfig> = [
    {
        key: 'email',
        type: 'input',
        className : "col-md-6",
        templateOptions: {
          label: 'sidenav.category',
          translate: true,
          placeholder: 'sidenav.category',
          required: true,
        }
    },
]