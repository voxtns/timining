export const capsule : Array<any> = [
  {
      name : "capsula.editGeneral",
      group : [
          {
              key: 'name',
              type: 'input',
              className : "col-md-6",
              templateOptions: {
                label: 'capsula.name',
                translate: true,
                placeholder: 'capsula.name',
                required: true,
              }
          },
          {
              key: 'avatar',
              type: 'input',
              className : "col-md-6",
              templateOptions: {
                label: 'capsula.avatar',
                translate: true,
                placeholder: 'capsula.avatar',
                required: true,
              }
          },
          {
              key: 'category',
              type: 'input',
              className : "col-md-12",
              templateOptions: {
                label: 'capsula.category',
                translate: true,
                placeholder: 'capsula.category',
                required: true,
              }
          },
          {
              key: 'description',
              type: 'textarea',
              className : "col-md-12",
              templateOptions: {
                label: 'capsula.description',
                translate: true,
                placeholder: 'capsula.description',
                required: true,
                rows: 5,
                grow: false
              }
          },
          {
              key: 'descriptionMin',
              type: 'textarea',
              className : "col-md-12",
              templateOptions: {
                label: 'capsula.descriptionMin',
                translate: true,
                placeholder: 'capsula.descriptionMin',
                required: true,
                rows: 5,
                grow: false
              }
          },
      ]
  },
  {
    name : "capsula.privateCapsula",
    group : [
      {
        key: 'video',
        type: 'video',
        className : "col-md-12",
        templateOptions: {
          label: 'capsula.video',
          translate: true,
          placeholder: 'capsula.video',
          required: true,
        }
      },
      {
        key: 'document',
        type: 'input',
        className : "col-md-6",
        templateOptions: {
          label: 'capsula.document',
          translate: true,
          placeholder: 'capsula.document',
          required: true,
        }
      }
    ]
  }
]

export const queryCapsule : Array<any> = [
  {
    key: 'query[#index].name',
    type: 'input',
    className : "col-md-12",
    templateOptions: {
      label: 'capsula.query.title',
      translate: true,
      placeholder: 'capsula.query.title',
      required: true,
    }
  },
  {
    key: 'query[#index].query',
    type: 'textarea',
    className : "col-md-12",
    templateOptions: {
      label: 'capsula.query.query',
      translate: true,
      placeholder: 'capsula.query.query',
      required: true,
      rows: 5,
      grow: false
    }
  },
  {
    key: 'query[#index].type',
    type: 'create',
    className : "col-md-12",
    templateOptions: {
      label: 'capsula.query.type',
      translate: true,
      placeholder: 'capsula.query.type',
      required: true,
    }
  },
]