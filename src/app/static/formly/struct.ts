export const struct : Array<object> = [
    {
        select : "formly.struct.text",
        struct : {
            type : "input",
            templateOptions : {
                type : "text",
                label : "#label",
                translate : true,
                placeholder : "#label"
            }
        }
    },
    {
        select : "formly.struct.number",
        struct : {
            type : "input",
            templateOptions : {
                type : "number",
                label : "#label",
                translate : true,
                placeholder : "#label"
            }
        }
    },
    {
        select : "formly.struct.email",
        struct : {
            type : "input",
            templateOptions : {
                type : "email",
                label : "#label",
                translate : true,
                placeholder : "#label"
            }
        }
    },
    {
        select : "formly.struct.radio",
        type : "options",
        struct : {
            type : "radio",
            templateOptions : {
                label : "#label",
                translate : true,
                options : [ ]
            }
        }
    },
    {
        select : "formly.struct.select",
        type : "options",
        struct : {
            type : "select",
            templateOptions : {
                label : "#label",
                translate : true,
                options : [ ]
            }
        }
    },
    {
        select : "formly.struct.selectMultiple",
        type : "options",
        struct : {
            type : "select",
            templateOptions : {
                multiple : true,
                label : "#label",
                translate : true,
                options : [ ]
            }
        }
    },
]