export const comment : Array<any> = [
    {
        name : "formly.editComment",
        group : [
            {
                key: 'fullName',
                type: 'input',
                className : "col-md-6",
                templateOptions: {
                  label: 'formly.fullName',
                  translate: true,
                  placeholder: 'formly.category',
                  required: true,
                }
            },
            {
                key: 'email',
                type: 'input',
                className : "col-md-6",
                templateOptions: {
                  label: 'formly.email',
                  translate: true,
                  placeholder: 'formly.email',
                  required: true,
                }
            },
            {
                key: 'web',
                type: 'input',
                className : "col-md-6",
                templateOptions: {
                  label: 'formly.web',
                  translate: true,
                  placeholder: 'formly.web'
                }
            },
            {
                key: 'comment',
                type: 'textarea',
                className : "col-md-6",
                templateOptions: {
                  label: 'formly.comment',
                  translate: true,
                  placeholder: 'formly.comment',
                  required: true,
                }
            },

        ]
    },
    {
        name : "formly.editComment",
        group : [
            {
                key: 'pros',
                type: 'input',
                className : "col-md-6",
                templateOptions: {
                  label: 'formly.pros',
                  translate: true,
                  placeholder: 'formly.pros',
                  required: true,
                }
            },
            {
                key: 'cons',
                type: 'input',
                className : "col-md-6",
                templateOptions: {
                  label: 'formly.cons',
                  translate: true,
                  placeholder: 'formly.cons',
                  required: true,
                }
            },

        ]
    },
]