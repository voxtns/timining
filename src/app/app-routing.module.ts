import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SessionComponent } from './pages/session/session.component';
import { LoginComponent } from './pages/session/login/login.component';

import { AdminComponent } from './pages/admin/admin.component';
import { StatusJobComponent } from './pages/admin/status-job/status-job.component';


const routes: Routes = [
  {
    path: '', 
    component: SessionComponent,
    children : [
      {
        path: '', 
        redirectTo: '/login',
        pathMatch: 'full' 
      },
      {
        path: 'login', 
        component: LoginComponent,
      },
    ]
  },
  { 
    path: 'admin', 
    component: AdminComponent,
    children : [
      {
        path: '', 
        component: StatusJobComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
