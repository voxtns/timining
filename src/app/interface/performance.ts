import { List } from './list';
import { Values } from './values';

export interface Performance {
    devices : List,
    values  : Values
}

export class Performance{
    constructor(){
        this.devices = new List();
        this.values = new Values();
    }
}