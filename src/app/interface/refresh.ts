export interface Refresh {
    devices : string,
    status  : string,
    date    : Date,
    view    ?: boolean;
}

export class Refresh{
    constructor(){
        this.date    = new Date();
        this.devices = "";
        this.status  = "";
        this.view    = false;
    }
}