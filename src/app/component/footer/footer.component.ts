import { Component, OnInit, Input } from '@angular/core';
import { global } from '../../static/global';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input("classFooter") classFooter : string;
  global : Object = global;
  
  constructor() { }

  ngOnInit(): void {
    
  }

}
