import { Component, OnChanges, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { CallService } from '../../service/call.service';
import { FormlyFormOptions } from '@ngx-formly/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-formly-card',
  templateUrl: './formly.component.html',
  styleUrls: ['./formly.component.scss']
})
export class FormlyComponent implements OnChanges, OnInit {
  @Input("model") model   : Object;
  @Input("fields") fields : Array<Object>
  @Output("initSubmit") initSubmit = new EventEmitter<Function>();

  form      : FormGroup;
  options   : FormlyFormOptions;
  items     : {
    model   : Object,
    type    : Object,
    fields  : Array<FormlyFieldConfig>,
  };
  optionsAsync : Object;

  constructor(private elem : ElementRef,private call : CallService) { 
    this.options  = {};
    this.optionsAsync = {};
    
    this.form     = new FormGroup({});
    this.items    = {
      model   : new Object(),
      type    : new Object(),
      fields  : new Array<FormlyFieldConfig>(),
    }; 
  }
  
  ngOnInit(){
//    this.transform();

  
    let submit = (e) => {
      this.elem.nativeElement.children[0].children[1].click();
      e({
        model   : this.items.model,
        fields  : this.items.fields
      })
      
      if(this.form.valid){
        e({
          model   : this.items.model,
          fields  : this.items.fields
        })
      }
    };
    this.initSubmit.emit(submit);
  }

  onSubmit(model){
    console.log(model)
    
  }
  
  ngOnChanges(changes): void {
    this.items.model = this.model;
    if(!(this.fields == null || this.fields.length == 0)){
      this.items.fields = this.transform();
      console.log(this.fields)
    }
  }
  
  private transform(){
    let items = new Array<FormlyFieldConfig>();

    this.fields.forEach(elem => {
      if(elem['async']){ 
        elem['async'].forEach(async => {
          this.call.get(async['api'], { }, -1)(data=>{
            let index = elem['group'].findIndex( e => e.key == async.key );
            let opts = new Array<any>();
            data.forEach(fr => {
                let obj = {};
                if(async['convertion']){
                  for(let conve in async['convertion']){
                    obj[conve] = fr[async['convertion'][conve]]
                  }
                }else{
                  obj = {
                    value : fr["id"],
                    label : fr["name"],
                  };
                }
              //obj['nature'] = fr;
              opts.push(obj);
            });
            elem['group'][index].templateOptions.options = opts;
          });
        });
      }
      
      if(elem['group']){
        items.push(
          {
            wrappers: ['card'],
            fieldGroupClassName: "row",
            templateOptions: { 
              label: elem['name'] 
            },
            fieldGroup : elem['group']
          }
        )
      }else{
        items.push(elem)
      }
    });

    return items;
  }


}
