import { Component, OnInit, Input, OnChanges} from '@angular/core';
import { YtPlayerService, PlayerOptions } from 'yt-player-angular';

@Component({
  selector: 'app-view-video',
  templateUrl: './view-video.component.html',
  styleUrls: ['./view-video.component.scss']
})
export class ViewVideoComponent implements OnInit, OnChanges {
  @Input("src") src : String;
  @Input("load-video") loadVideo : boolean;
  loader : boolean;
  url : String;
  compare : String;
  options : PlayerOptions;
  show    : boolean;
  status  : number;

  constructor(public ytPlayerService: YtPlayerService){
    this.status = 0;
    this.options = {
      controls : false,
      keyboard :  false,
      fullscreen :  false,
      annotations : false,
      info : false
    };
    this.show = true;
  }
 
  ngOnChanges() : void {
    this.show = true;
    this.status = 0;
    this.compare = this.idUrl(this.src);
    if(this.url != this.compare ){
      this.loader = true;
      this.url = this.compare;
      setTimeout(_=>{
        this.loader = false;
      },0);
      console.log(this.url, this.loader, this.url != this.compare)
    }
  }

  ngOnInit(): void {
    console.log(this.loadVideo)
    if(typeof this.loadVideo == 'undefined'){
      this.loadVideo = true;
    }
    this.ngOnChanges();
  }

  private idUrl(src) : String{
    let tr = "";
    if(src != null) {
      if(src.indexOf("youtube") > -1){
        if(src.indexOf("/watch?v=") > -1){
          tr = src.split("&")[0].replace("/watch?v=", "/embed/").split("/embed/");
        }else if(src.indexOf("/embed/") > -1){
          tr = src.split("/embed/");
        }
        tr = tr[1];
      }
    }
    return tr;
  }

  play(status){
    console.log(this.ytPlayerService) 
    if(status == 0){
      this.ytPlayerService.play();
      this.status = 1;
    }else{
      this.ytPlayerService.pause();
      this.status = 0;
    }
    setTimeout(() => {
      this.show = false;
    }, 250);
  }
}
