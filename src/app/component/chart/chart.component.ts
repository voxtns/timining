import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  ViewChild,
  OnChanges,
} from '@angular/core';

import { Performance } from '../../interface/performance';
import Chart from 'chart.js';

@Component({
  selector: 'app-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements AfterViewInit, OnChanges {
  @ViewChild('myAreaChart') myAreaChart!: ElementRef<HTMLCanvasElement>;
  @Input() duration: 'monthly' | 'quarterly' | 'yearly' = 'yearly';
  @Input() height!: string;
  @Input() width!: string;
  @Input() data : Performance;
  chart!: Chart;

  constructor() { 
    Chart.defaults.global.defaultFontFamily = 'Metropolis,-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';
  }

  ngAfterViewInit(): void {
    this.chart = new Chart(this.myAreaChart.nativeElement, {
      type: 'bar',
      data: {
          datasets: [
              {
                  label: 'Revenue',
                  backgroundColor: 'rgba(0, 97, 242, 1)',
                  hoverBackgroundColor: 'rgba(0, 97, 242, 0.9)',
                  borderColor: '#4e73df',
                  maxBarThickness: 25,
              },
          ],
      },
      options: {
          maintainAspectRatio: false,
          layout: {
              padding: {
                  left: 10,
                  right: 25,
                  top: 25,
                  bottom: 0,
              },
          },
          scales: {
              xAxes: [
                  {
                      gridLines: {
                          display: false,
                          drawBorder: false,
                      },
                      ticks: {
                          maxTicksLimit: 6,
                      },
                  },
              ],
              yAxes: [
                  {
                      ticks: {
                          min: 0,
                          max: 1,
                          maxTicksLimit: 5,
                          padding: 10,
                          callback: (value, index, values) => {
                              return value * 100 + '%';
                          },
                      },
                      gridLines: {
                          color: 'rgb(234, 236, 244)',
                          zeroLineColor: 'rgb(234, 236, 244)',
                          drawBorder: false,
                          borderDash: [2],
                          zeroLineBorderDash: [2],
                      },
                  },
              ],
          },
          legend: {
              display: false,
          },
          tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: 'rgb(255,255,255)',
              bodyFontColor: '#858796',
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                  label: (tooltipItem, chart) => {
                      let datasetLabel = '';
                      if (chart && chart.datasets) {
                          datasetLabel = chart.datasets[tooltipItem.datasetIndex as number]
                              .label as string;
                      }
                      return (
                          datasetLabel +
                          ': ' +
                          Math.round(tooltipItem.yLabel * 10000) / 100 + '%'
                      );
                  },
              },
          },
      },
    });
  }

  ngOnChanges(): void{
    if(typeof this.chart !== 'undefined'){
      this.chart.data.labels = this.data.devices;
      this.chart.data.datasets[0].data = this.data.values;
      this.chart.update();
    }
  }

}
