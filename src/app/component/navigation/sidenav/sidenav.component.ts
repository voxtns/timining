import { Component, OnInit, EventEmitter, Output }  from '@angular/core';
import { menu } from '../../../static/menu';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @Output() navClickSelect = new EventEmitter<boolean>();
  menu : Array<Object>;
  select : Object;
  constructor() { 
    this.menu = menu;
  }

  ngOnInit(): void {

    console.log(this.menu)
  }

  navSelect(e, f){
    if(e.isActive){
      if(this.select != f){
        this.navClickSelect.emit(f);
      }
      this.select = f;
    }

    return "active";
  }
} 
