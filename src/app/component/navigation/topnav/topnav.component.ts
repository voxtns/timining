import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AccionService } from '../../../service/accion.service';
import { CallService } from '../../../service/call.service';
import { global } from '../../../static/global';
import { Router } from '@angular/router'

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {
  @Output() sidenavToggled = new EventEmitter<boolean>();
  show : boolean;
  global : Object = global;
  user : {};

  constructor(private call : CallService,private action : AccionService, private router : Router) { 
    this.show = false;
    this.user = {};
    if(this.call.auth()){
      this.user = this.call.storage.get('auth');
      console.log(this.user)
    }

  }

  ngOnInit(): void {
  }

  fullClk(){
    this.show = !this.show;
    this.sidenavToggled.emit(this.show);    
  }

  logout(){
    this.action.warning("logout", this.user).then(e=>{
      if(e){
        //this.call.logout();
      }
    });
    //this.action.warning()
  }
}
