import { Component, OnInit, Input }               from '@angular/core';
import { filter }                                 from 'rxjs/operators';
import { Location }                               from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd }  from '@angular/router';

@Component({
  selector: 'app-nav-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input("hearderNavSelect") hearderNavSelect : Object;

  search : string;
  navegation : boolean;  

  constructor(private location: Location, private router : Router,private activatedRoute : ActivatedRoute) { 
    this.navegation = false;

    
    this.activatedRoute.queryParams.subscribe(e=>{
      if(this.search != e['q']) {
        this.search = e.q;
      }
    });
  } 

  ngOnInit(): void {

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)).subscribe((e) => {
        
      }
    );

    this.activatedRoute.params.subscribe(e=>{
      if(e['uuid']){
        this.navegation = true;
      }else{
        this.navegation = false;
      }
    });
  }

  backClicked(search) : void {
    if(search == ""){
      this.router.navigate([this.router.url.split("?")[0]]);
    }else{
      this.router.navigate([this.router.url.split("?")[0]],{
        queryParams : {
          q : search,
        }
      })
    }
  }
}
