import { Component, HostListener } from '@angular/core';
import { LoadingpageService } from '../../../service/loadingpage.service';

@Component({
  selector: 'app-button-up',
  templateUrl: './button-up.component.html',
  styleUrls: ['./button-up.component.scss']
})
export class ButtonUpComponent {
  private showScrollHeight = 400;
  private hideScrollHeight = 200;
  showGoUpButton: boolean;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (( window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > this.showScrollHeight) {
        this.showGoUpButton = true;
    } else if ( this.showGoUpButton && (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < this.hideScrollHeight) {
      this.showGoUpButton = false;
    }
  }

  constructor(private load : LoadingpageService) { 
    this.showGoUpButton = false;
  }

  scrollTop() {
    this.load.scrollMov(document.body);
  }
}
