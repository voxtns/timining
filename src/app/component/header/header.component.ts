import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { global } from '../../static/global';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'] 
})
export class HeaderComponent implements OnInit {
  title : Object;
  @ViewChild("start") start: TemplateRef<any>;
  @ViewChild("landing") landing: TemplateRef<any>;
  @ViewChild("landing1") landing1: TemplateRef<any>;
  @ViewChild("landing2") landing2: TemplateRef<any>;
  @ViewChild("landing3") landing3: TemplateRef<any>;
  @ViewChild("landing4") landing4: TemplateRef<any>;

  constructor(router : Router) { 
    this.title = new Object();
    this.title["template"] = false;
    router.events.subscribe(val => {
      if(val instanceof NavigationEnd){
        setTimeout(() => {
          let temp = new Object();
          temp = this.search(val.url, global.navbar);
          if(temp == null){
            temp = this.search(val.url, global.button);
            if(temp != null){
              this.title = temp;
            }else{
              this.title = new Object();
              this.title["template"] = false;
            }
          }else{
            this.title = temp;
          }
        },0)
      }
    })
  }

  ngOnInit(): void {

  }

  private search(ary, nav){
    let obj = null;
    nav.forEach(e=>{
      if(obj == null){
        if(e['url']){
          if(e.url == ary){
            obj = e;
          }
        }else{
          if(e['children']){
            obj = this.search(ary, e.children);
          }else{
            obj = this.search(ary, e.nav);
          }
        }
      }
    });
    return obj;
  }
}
