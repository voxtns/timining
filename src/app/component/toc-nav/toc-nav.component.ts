import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ElementRef, HostListener } from '@angular/core';
import { LoadingpageService } from '../../service/loadingpage.service';

@Component({
  selector: 'app-toc-nav',
  templateUrl: './toc-nav.component.html',
  styleUrls: ['./toc-nav.component.scss']
})
export class TocNavComponent implements OnInit, OnChanges {

  @Input("fields") fields : Array<{ name : string }>
  @Input("scrollSpy") scrollSpy : ElementRef;

  currentSection;
  constructor(private _el: ElementRef, private load : LoadingpageService) {

  }

  @HostListener('window:scroll', ['$event'])

  onScroll(event: any) {
    let scrollSpy = this.scrollSpy_();
    
    let currentSection: string;
    let scrollTop = event.target.scrollingElement.scrollTop;
    let parentOffset = event.target.scrollingElement.offsetTop;
    for (let i = 0; i < scrollSpy.length; i++) {
      const element = scrollSpy[i];
      if (element.tagName == "DIV") {
        if ((element.offsetTop - parentOffset) <= scrollTop) {
          currentSection = this.fields[i].name;
        }
      }
    }
    if (currentSection !== this.currentSection) {
      this.currentSection = currentSection;
      //this.sectionChange.emit(this.currentSection);
    }
  }

  ngOnChanges(){
    this.currentSection = this.fields[0].name;
  }

  ngOnInit(): void {
    this.currentSection = this.fields[0].name;
  }

  onSectionChange(section) {
    this.currentSection = section.name;
    this.load.scrollMov(this.scrollSpy_()[this.fields.findIndex( e => e.name == section.name)]);
  }

  private scrollSpy_(){
    let scrollSpy_ = this.scrollSpy['nativeElement'];
    if(typeof scrollSpy_ === "undefined"){
      scrollSpy_= this.scrollSpy['elem']['nativeElement'].querySelectorAll(".scrollSpy")
    }else{
      scrollSpy_ = this.scrollSpy.nativeElement.querySelectorAll(".scrollSpy")
    }

    return scrollSpy_;
  }
}
