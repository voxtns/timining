import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TocNavComponent } from './toc-nav.component';

describe('TocNavComponent', () => {
  let component: TocNavComponent;
  let fixture: ComponentFixture<TocNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TocNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TocNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
