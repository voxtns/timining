import { Component, OnInit, Input } from '@angular/core';
import { global } from '../../static/global';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input("navTopClass") navTopClass : string = "bg-transparent navbar-dark"; 
  options : Object;
  general  : Object; 
  constructor() { 
    this.general = global;
  }

  ngOnInit(): void {

  }

}
