import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { CallService } from '../../service/call.service';
import { Observable, pipe } from 'rxjs';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent extends FieldType implements OnInit {

  constructor(private call : CallService){
    super();

  }

  ngOnInit(){
    //console.log(this.model, this.field.key)
    if(typeof this.model[this.field.key] == undefined){
      this.model[this.field.key] = new Array<string>();
    }

    if(typeof this.field.templateOptions.autocomplete === 'undefined'){
      this.field.templateOptions.autocomplete = false;
    }
  }

  requestAutocompleteItems = (text: string) : Observable<any> => {
    return new Observable(ob=>{
      this.call.get(this.field.templateOptions.autocompleteObservable, { "keyword" : text },false)(e=>{
        ob.next(e);
      })
    });
  }
}
