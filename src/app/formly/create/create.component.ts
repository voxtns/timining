import { Component, OnInit, OnChanges }  from '@angular/core';
import { FieldType }          from '@ngx-formly/core';
import { struct }             from '../../static/formly/struct';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent extends FieldType implements OnInit, OnChanges {
  struct : Array<object> = struct;
  items  : Object = new Object();
  object_ : string;
  formly_ : Array<Object>;
  formly : Array<Object>;

  ngOnInit(): void {

    if(typeof this.formControl.value == "undefined" || this.formControl.value == null){
      this.items = {};
      this.formControl.setValue("");
    }else{
      console.log(this.formControl)
      this.items = this.formControl.value;
      //this.items = this.model[this.field.key];
    }
  }

  ngOnChanges() {
    console.log("asdasd")

  }

  add(items : Array<object>, index){
    let formly = Array<object>();
    this.formly = null;
    this.formly_ = Array<object>();
    items.forEach((item : { select, label }) => {
      if(typeof item['select'] !== "undefined"){
        let copy = JSON.parse(JSON.stringify(item.select.struct));
        copy.key = this.create_UUID();
        copy.className = "col-md-12";
        if(typeof copy['templateOptions'] !== 'undefined'){
          let stringify = JSON.stringify(copy['templateOptions']);
          stringify = stringify.replace(/#label/g, item.label);
          copy['templateOptions'] = JSON.parse(stringify);
        }else{
          copy['templateOptions'] = {};
        }

        if(item.select['type'] == 'options'){
          copy.templateOptions.options = item['options'];
        }

        formly.push(copy);
      }
    });

    setTimeout(_=>{
      this.formly  = JSON.parse(JSON.stringify(formly));
      this.formly_ = formly;
    }, 0)
    if(index == items.length - 1){
      this.items = {}
    }
  }

  addOptions(item, opt, index){
    if(opt.label.length > 0 && index == item.options.length - 1){
      item.options[item.options.length - 1]['value'] = item.options.length - 1;
      item.options.push({});
    }
    this.formControl.setValue("");
    this.formControl.setValue(this.items);
  }

  change(item){
    if(item.select['type'] == "options"){
      if(!item['options']){
        item['options'] = new Array<{label : string, value : string}>();
        item.options.push({});
      }
    }else{
      delete item['options'];
    }
    this.formControl.setValue("");
    this.formControl.setValue(this.items);
  }

  private create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
  }

  update(items){
    console.log(this.items, this.formly_)
  }
  compareFn(val1, val2): boolean {
    return val1 && val2 ? val1.select === val2.select : val1 === val2;
  }
}
