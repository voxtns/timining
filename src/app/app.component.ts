import { Component } from '@angular/core';
import { LoadingpageService } from './service/loadingpage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'TIMining';

  constructor(load : LoadingpageService,){
    load.init();
  }
  
}
