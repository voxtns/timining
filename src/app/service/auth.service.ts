import { Injectable } from '@angular/core';
import { AngularFireDatabase }  from '@angular/fire/database';
import { AngularFireAuth }      from '@angular/fire/auth';
import { auth }                 from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState: any = null;

  constructor(
    private afAuth  : AngularFireAuth,
    private db      : AngularFireDatabase,
  ) {

  }

  // Return is init 

  init(e : Function): void {
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
      e((this.authState !== null ? this.authState['email'] !== null : false));
    });
  }
  
  // Returns true if user is logged in
  get authenticated(): boolean {
    return this.authState !== null;
  }

  // Returns current user data
  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  // Returns
  get currentUserObservable(): any {
    return this.afAuth.authState
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  // Anonymous User
  get currentUserAnonymous(): boolean {
    return this.authenticated ? this.authState.isAnonymous : false
  }

  // Returns current user display name or Guest
  get currentUserDisplayName(): string {
    if (!this.authState) { return 'Guest' }
    else if (this.currentUserAnonymous) { return 'Anonymous' }
    else { return this.authState['displayName'] || 'User without a Name' }
  }

  //// Social Auth ////
  githubLogin() {
    const provider = new auth.GithubAuthProvider()
    return this.socialSignIn(provider);
  }

  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.socialSignIn(provider);
  }

  facebookLogin() {
    const provider = new auth.FacebookAuthProvider();
    return this.socialSignIn(provider);
  }

  twitterLogin(){
    const provider = new auth.TwitterAuthProvider();
    return this.socialSignIn(provider);
  }

  private socialSignIn(provider) {
    return this.afAuth.signInWithPopup(provider).then((credential) =>  {
        this.authState = credential.user
        this.updateUserData()
    }).catch(error => {
      if(error['code'] != "auth/popup-closed-by-user"){
       // this.all.swal(error['code'], error['message']).alert();
      }
      console.log(error);
    });
  }


  //// Anonymous Auth ////
  anonymousLogin() {
    return this.afAuth.signInAnonymously()
    .then((user) => {
      this.authState = user
      this.updateUserData()
    })
    .catch(error => console.log(error));
  }

  //// Email/Password Auth ////
  emailSignUp(email:string, password:string, name:string = "") {
    return this.afAuth.createUserWithEmailAndPassword(email, password).then((user) => {
      
      user.user.updateProfile({
        displayName : name,
        photoURL    : "" //domainConfig.img+domainConfig.default.imgUser
      }).then(() => {
        this.authState = user.user;
        this.updateUserData();
        this.signOut();
        this.emailLogin(email, password);
      }, error => console.log(error));

      }).catch(error => console.log(error));
  }

  userExists(email:string){
    return {
      then : e =>{
        /*return this.send.get('login','issetUser',email).then(f=>{
          if(f['status']){
            e(f.data);
            return f.data;
          }
        })*/
      }
    }
  }

  emailLogin(email:string, password:string) {
     return {
       then : e =>{
          this.afAuth.signInWithEmailAndPassword(email, password)
          .then((user) => {
            this.authState = user.user;
            this.updateUserData();
            e({
              status  : true,
              data    : user.user
            });
          })
          .catch(error => {
            //this.all.swal(error['code'], error['message']).alert();
            console.log(error);
            e({
              status  : false,
              data    : error
            });
          });
       }
     };
  }

  // Sends email allowing user to reset password
  resetPassword(email: string) {
    return this.afAuth.sendPasswordResetEmail(email)
      .then(() => console.log("email sent"))
      .catch((error) => console.log(error))
  }


  //// Sign Out ////
  signOut(): void {
    this.afAuth.signOut();
    //this.navCtrl.setRoot(SystemLoginPage);
  }

  userData(): Object {
    return this.authState;
  }

  updateDataSession(data){
    this.updateUserData(data);
  }

  getUserData(e : Function, var_ : string = null) : void{
    let path = 'users/'+this.currentUserId + (var_ !== null ? '/' + var_ : ''); 
    this.db.object(path).valueChanges().subscribe(f=>e(f));
  }

  //// Helpers ////
  private updateUserData(data : Object = new Object): void {
    let path = 'users/'+this.currentUserId; // Endpoint on firebase
    data['email'] = this.authState.email;
    data['name']  = this.authState.displayName;    
    this.db.object(path).update(data).catch(error => console.log(error));
  }
}
