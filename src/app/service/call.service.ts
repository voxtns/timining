import { Injectable }                 from '@angular/core';
import { HttpClient }                 from '@angular/common/http';
import { Router }                     from '@angular/router';
import { NgbModal, NgbModalRef }      from '@ng-bootstrap/ng-bootstrap';
import { from }                       from 'rxjs';
import { environment }                from '../../environments/environment';
import { LoadingComponent }           from '../component/navigation/loading/loading.component';

declare const $ : any;

@Injectable({
  providedIn: 'root'
})
export class CallService {
  token   : string | boolean;
  loadingStatus : {
    load  : number,
    modal ?: NgbModalRef
  };

  storage : {
    isset   : Function,
    get     : Function,
    set     : Function
  };
  base64  : {
    encode  : Function,
    decode  : Function,
    tra     : Function,
    des     : Function
  };

  constructor(
    private http            : HttpClient, 
    private router          : Router,
    private modal           : NgbModal
  ) { 
    this.loadingStatus = { load : 0 };
    this.modal.dismissAll();
    this.base64 = {
      encode : function(key : string){
        return btoa(unescape(encodeURIComponent(key))).replace('/', '_');
      },
      decode : function(e : string){
        let result : any;
        try{
          result = decodeURIComponent(escape(atob(e.replace('_','/'))));
        }catch(e){
          result = false;
        }
        return result;
      },
      tra : function(data : any){
        return this.encode(JSON.stringify(data));
      },
      des : function(key : string){
        return JSON.parse(this.decode(key));
      }
    }

    this.storage = {
      isset : (key : string) : boolean => {
        return !!localStorage.getItem(key);
      },
      get : (key : string) : any => {
        return this.base64.des(localStorage.getItem(key));
      }, 
      set : (key : string, data : any) => {
        localStorage.setItem(key,this.base64.tra(data));
      },
    }
    
  }
  
  private options(params : {}, get : boolean = true) {
    
    let options_ = {
      headers : {
        'Content-Type' : 'application/javascript',
      }
    };
    if(get){
      options_['params'] = params; 
    }
    if(this.auth()){
      //options_.headers["Authorization"] =  "Bearer "+this.auth();
    }
    return options_;
  }

  auth(){
    return localStorage.getItem("auth");
  }

  get(api : Array<string | number>, params : {} = null, loading : boolean | number = true){
    return (success) => {
      /*this.http.get( environment.endpoint + api.join("/"), this.options(params) ).subscribe(data => {
        success({
          status  : true,
          data    : data
        });
      });*/
      this.loading(loading);
      let get = "";
      for(let key in params ){
        get += "&"+key+"="+params[key]
      }
      let opt = this.options(params);
      let result = from(
        fetch(
          environment.endpoint + api.join("/")+get,
          {
            headers: opt.headers,
            mode: 'cors',
            method: 'GET'
          }
        )
      );
      result.subscribe(data=>{
        data.json().then(data=>{
          this.loading(false);
          if(this.logoutIf(data['status'])){
            this.logout();
          }
          else{
            success(data);     
          }
        })
      })
    }
  }

  post(api : Array<string | number>, params : {} = null, loading : boolean | number = true){
    return (success) => {
      this.loading(loading);
      let opt = this.options(params);
      
      fetch(
        environment.endpoint + api.join("/"),
        {
          body: JSON.stringify(params),
          headers: opt.headers,
          method: 'POST',
          redirect: 'follow'
        }
      ).then(data=>{
        data.json().then(data=>{
          this.loading(false);
          if(this.logoutIf(data['status'])){
            this.logout();
          }else{
            success(data);     
          }         
        })
      })
    }
  }

  private logoutIf(status){
    return status == 'Authorization Token not found' || status == "Token is Expired" || status == 'Token is Invalid'
  }

  loading(status: boolean | number){
    if(status != -1) {
      if(status){
        this.loadingStatus.load ++;
        if(this.loadingStatus.load == 1){
          if(this.modal.hasOpenModals()){
            this.modal.dismissAll();
          }
          this.loadingStatus['modal'] = this.modal.open(LoadingComponent, {
            windowClass: 'modal-loading',
            backdrop: false
          }); 
        }
      }else{
        this.loadingStatus.load --;
        if(this.loadingStatus.load < 1){
          this.loadingStatus.load = 0;
          this.loadingStatus.modal.close()
        }
      }
    }
  }

  logout(){
    localStorage.removeItem("auth");
    this.router.navigate(["/login"]);
  }

  socket() : { then : Function }{
    let socket = new WebSocket(environment.wws);
    return {
      then : (event_) => {
        socket.addEventListener('open', function (event) {
          socket.addEventListener('message', function (event) {
            event_(event.data)
          });
        });
      }
    };
  }
}
