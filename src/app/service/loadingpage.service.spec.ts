import { TestBed } from '@angular/core/testing';

import { LoadingpageService } from './loadingpage.service';

describe('LoadingpageService', () => {
  let service: LoadingpageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadingpageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
