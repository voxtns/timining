import { Injectable } from '@angular/core';

declare var AOS : any;
declare var jQuery : any;

@Injectable({
  providedIn: 'root'
})
export class LoadingpageService {
    constructor() { 

    }

    init(){
        (function($) {
            "use strict";

            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
            $(".popover-dismiss").popover({
                trigger: "focus"
            });

            var navbarCollapse = function() {
                if($(".navbar-marketing.bg-transparent.fixed-top").length === 0) {
                    return;
                }
                if ($(".navbar-marketing.bg-transparent.fixed-top").offset().top > 0) {
                    $(".navbar-marketing").addClass("navbar-scrolled");
                } else {
                    $(".navbar-marketing").removeClass("navbar-scrolled");
                }
            };

            navbarCollapse();
            $(window).scroll(navbarCollapse);
    })(jQuery);  

    AOS.init({
        disable: 'mobile',
        duration: 600,
        once: true
    });
    }

    scrollMov(scroll){
        jQuery('html, body').animate({
            scrollTop: jQuery(scroll).offset().top - 100
        }, 250);
    }
}
