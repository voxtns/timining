import { Injectable } from '@angular/core';
import { NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import Swal from'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AccionService {

  constructor(private alertConfig: NgbAlertConfig, private translate : TranslateService) { }

  delete(item: { title : string }, serv : string = "", return_ : Function = (e) => {}){
    Swal.fire({
      title: 'Estas seguro de eliminar?',
      text: 'Estas seguro de eliminar el registro '+item.title,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Eliminar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.messenger("success","Eliminado correctamente")
        return_("sucess")
      } else if (result.dismiss === Swal.DismissReason.cancel) {

        return_("error")
      }
    })
  }

  warning( text : string, data : any = null){
    return {
      then : return_ => {
        if(this.translate.store.translations[this.translate.store.defaultLang]["swal"]){
          text = "swal." + text;
          this.translate.get([text+".text", text+".title", text+".confirmButtonText", text+".cancelButtonText", text+".success"], data).subscribe(e=>{
            Swal.fire({
              title: e[text+".title"],
              text: e[text+".text"],
              icon: 'warning',
              showCancelButton: true,
              confirmButtonText: e[text+".confirmButtonText"],
              cancelButtonText: e[text+".cancelButtonText"]
            }).then((result) => {
              if (result.value) {
                if(e[text+".success"] != text+".success"){
                  this.messenger("success",e[text+".success"])
                }
                return_(true)
              } else if (result.dismiss === Swal.DismissReason.cancel) {
                return_(false)
              }
            })
          })
        }
      }
    }
  }

  private messenger(type : string, text : string, time : number = 1.5){
    var tag = document.createElement("div");
    tag.appendChild(document.createTextNode(text));
    tag.classList.add("alert");
    tag.classList.add("alert-"+type);
    tag.style.position = "fixed";
    tag.style.right = "15px";
    tag.style.bottom = "0px";
    document.body.appendChild(tag);

    setTimeout(_=>{
      tag.remove()
    }, time * 1000)
  }
}
