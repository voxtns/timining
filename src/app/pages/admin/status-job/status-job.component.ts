import { Component, OnInit } from '@angular/core';
import { CallService } from '../../../service/call.service';
import { List } from '../../../interface/list';
import { Performance } from '../../../interface/performance';
import { Order } from '../../../interface/order';
import { Refresh } from '../../../interface/refresh';

@Component({
  selector: 'app-status-job',
  templateUrl: './status-job.component.html',
  styleUrls: ['./status-job.component.scss']
})
export class StatusJobComponent implements OnInit {
  check_   : Array<String>;

  tabGraphics : boolean;
  lists : List;
  performance : Performance;
  orderBy : Order[];
  refresh : Object;

  constructor(private call : CallService) { 
    this.tabGraphics = true;
    this.lists = new List();
    this.performance = new Performance();
    this.orderBy = new Array<Order>();
    this.refresh = Object();
    this.check_  = Array<String>();

  }

  ngOnInit(): void {
    this.call.get(["device","list"])((lists : List)=>{
      this.lists = lists;
    });

    this.loop();

    this.call.socket().then((refresh : String)=>{
      let spl : Array<string> = refresh.split("|");
      let refresh_ : Refresh = {
        devices : spl[0],
        date    : new Date(),
        status  : spl[1]
      }
      
      if(typeof this.refresh[refresh_.devices] === 'undefined'){
        this.refresh[refresh_.devices] = new Array<Refresh>();
      }
      this.refresh[refresh_.devices].unshift(refresh_);
    });
  }

  check(value : string){
    if(this.check_[value]){
      this.check_[value] = false;
    }else{
      this.check_[value] = true;
    }
  }

  private loop(load : boolean = true) {
    this.call.get(["device","performance"], {}, load)((performance : Performance)=>{
      this.performance = performance;
      this.orderBy     = this.order(performance);
    });

    setTimeout(_=>{
      this.loop(false);
    }, 5000)
  }

  private order(performance : Performance) : Order[]{
    let order : Array<Order> = new Array<Order>();

    performance.values.forEach((element, index) => {
      order.push({
        values : Math.round(element * 10000) / 100,
        devices : performance.devices[index]
      });
    });

    order.sort((a : Order, b : Order ) =>{
      if (a.values > b.values) {
        return 1;
      }
      if (a.values < b.values) {
        return -1;
      }
      return 0;
    });

    return order;
  }
}
