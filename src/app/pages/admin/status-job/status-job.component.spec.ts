import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusJobComponent } from './status-job.component';

describe('StatusJobComponent', () => {
  let component: StatusJobComponent;
  let fixture: ComponentFixture<StatusJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
