import { Component, OnInit } from '@angular/core';
import { LoadingpageService } from '../../service/loadingpage.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  sidenavToggled : boolean;
  menuSelect : Object;
  constructor(private load : LoadingpageService, router: Router) { 
    let html = document.getElementsByTagName('html')[0];
    html.classList.remove("app-public");
    html.classList.add("app-admin");
  }

  ngOnInit(): void {
    this.menuSelect = new Object();
  }

  showClk(e){
    this.sidenavToggled = e;
  }

  navClickSelect(e){
    console.log(e)
    this.menuSelect = e;
  }
}
