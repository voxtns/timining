import { Component, OnInit } from '@angular/core';
import { login } from '../../../static/formly';
import { global } from '../../../static/global';
import { CallService } from '../../../service/call.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  fields : Array<Object>;
  model : Object;
  global : Object;
  submit : Function;

  constructor(private call : CallService, private router : Router) {
    this.model  = new Object(); 
    this.fields = login;
    this.global = global;
    if(this.call.auth()){
      this.router.navigate(["/admin"]);
    }
  }

  ngOnInit(): void {
    /*this.auth.init(e=>{
      if(e){
        this.router.navigate(["/","admin"])
      }
    });*/
  }

  initSubmit(e){
    this.submit = e;
  }

  onSubmit(){
    this.submit((e : { model : { email : string, password : string } }) => {
      this.call.post(["login"],e.model)(e=>{
        this.call.storage.set("auth", e);
        this.router.navigate(["/admin"]);
      })
      console.log(e)
    })
  }
}
