import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {

  constructor() { 
    let html = document.getElementsByTagName('html')[0];
    html.classList.remove("app-public");
    html.classList.add("app-admin");
  }

  ngOnInit(): void {
  }

}
