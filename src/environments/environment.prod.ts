export const environment = {
  production: true,
  endpoint  : "https://frontend-excercise.dt.timlabtesting.com/ops/",
  wws       : "wss://frontend-excercise.dt.timlabtesting.com/eventstream/connect",
  base      : "/",
  firebase  : {

  },
  translate : {
    json : ["./assets/i18n/", ".json"],
    default : "es"
  }
};
